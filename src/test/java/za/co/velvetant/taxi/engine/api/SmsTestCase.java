package za.co.velvetant.taxi.engine.api;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SmsTestCase {

    @Test
    public void test() throws JsonProcessingException {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        assertNotNull(objectMapper.writeValueAsString(new Sms(new Sms.Message("0835540735", "ref1", "you've won a million dollars!"))));
    }

}
