package za.co.velvetant.taxi.statistics;

import za.co.velvetant.taxi.engine.api.Base;

public class Totals extends Base {

    private Long onlineDrivers;
    private Long availableDrivers;
    private Long engagedDrivers;
    private Long trips;
    private Long cancelledByCustomer;
    private Long cancelledByDriver;
    private Long cancelledByOps;
    private Long notAcceptedTrips;
    private Long averageDuration;
    private Long totalDuration;
    private Long averageDistance;
    private Long totalDistance;

    private Double totalFareCosts;
    private Double totalClipCost;

    public Long getOnlineDrivers() {
        return onlineDrivers;
    }

    public void setOnlineDrivers(final Long onlineDrivers) {
        this.onlineDrivers = onlineDrivers;
    }

    public Long getAvailableDrivers() {
        return availableDrivers;
    }

    public void setAvailableDrivers(final Long availableDrivers) {
        this.availableDrivers = availableDrivers;
    }

    public Long getEngagedDrivers() {
        return engagedDrivers;
    }

    public void setEngagedDrivers(final Long engagedDrivers) {
        this.engagedDrivers = engagedDrivers;
    }

    public Long getTrips() {
        return trips;
    }

    public void setTrips(final Long trips) {
        this.trips = trips;
    }

    public Long getCancelledByCustomer() {
        return cancelledByCustomer;
    }

    public void setCancelledByCustomer(final Long cancelledByCustomer) {
        this.cancelledByCustomer = cancelledByCustomer;
    }

    public Long getNotAcceptedTrips() {
        return notAcceptedTrips;
    }

    public void setNotAcceptedTrips(final Long notAcceptedTrips) {
        this.notAcceptedTrips = notAcceptedTrips;
    }

    public Long getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(final Long totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Double getTotalFareCosts() {
        return totalFareCosts;
    }

    public void setTotalFareCosts(final Double totalFareCosts) {
        this.totalFareCosts = totalFareCosts;
    }

    public Double getTotalClipCost() {
        return totalClipCost;
    }

    public void setTotalClipCost(final Double totalClipCost) {
        this.totalClipCost = totalClipCost;
    }

    public Long getCancelledByDriver() {
        return cancelledByDriver;
    }

    public void setCancelledByDriver(final Long cancelledByDriver) {
        this.cancelledByDriver = cancelledByDriver;
    }

    public Long getCancelledByOps() {
        return cancelledByOps;
    }

    public void setCancelledByOps(final Long cancelledByOps) {
        this.cancelledByOps = cancelledByOps;
    }

    public Long getAverageDistance() {
        return averageDistance;
    }

    public void setAverageDistance(final Long averageDistance) {
        this.averageDistance = averageDistance;
    }

    public Long getAverageDuration() {
        return averageDuration;
    }

    public void setAverageDuration(final Long averageDuration) {
        this.averageDuration = averageDuration;
    }

    public Long getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(final Long totalDuration) {
        this.totalDuration = totalDuration;
    }
}
