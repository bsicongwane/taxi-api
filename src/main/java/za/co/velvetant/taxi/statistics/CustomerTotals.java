package za.co.velvetant.taxi.statistics;

import za.co.velvetant.taxi.engine.api.Base;

public class CustomerTotals extends Base {

    private Long totalTrips;
    private Long totalCompletedTrips;
    private Long totalNonCompletedTrips;
    private Double totalSpent;
    private Long snappMiles;
    private Double totalCreditCardPayment;
    private Long totalTripRatings;
    private Long totalDriverRatings;
    private Long averageTripRating;
    private Long averageDriverRating;

    public Long getTotalTrips() {
        return totalTrips;
    }

    public void setTotalTrips(final Long totalTrips) {
        this.totalTrips = totalTrips;
    }

    public Long getTotalNonCompletedTrips() {
        return totalNonCompletedTrips;
    }

    public void setTotalNonCompletedTrips(final Long totalNonCompletedTrips) {
        this.totalNonCompletedTrips = totalNonCompletedTrips;
    }

    public Long getTotalCompletedTrips() {
        return totalCompletedTrips;
    }

    public void setTotalCompletedTrips(final Long totalCompletedTrips) {
        this.totalCompletedTrips = totalCompletedTrips;
    }

    public Double getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(final Double totalSpent) {
        this.totalSpent = totalSpent;
    }

    public Long getSnappMiles() {
        return snappMiles;
    }

    public void setSnappMiles(final Long snappMiles) {
        this.snappMiles = snappMiles;
    }

    public Double getTotalCreditCardPayment() {
        return totalCreditCardPayment;
    }

    public void setTotalCreditCardPayment(final Double totalCreditCardPayment) {
        this.totalCreditCardPayment = totalCreditCardPayment;
    }

    public Long getTotalTripRatings() {
        return totalTripRatings;
    }

    public void setTotalTripRatings(final Long totalTripRatings) {
        this.totalTripRatings = totalTripRatings;
    }

    public Long getTotalDriverRatings() {
        return totalDriverRatings;
    }

    public void setTotalDriverRatings(final Long totalDriverRatings) {
        this.totalDriverRatings = totalDriverRatings;
    }

    public Long getAverageTripRating() {
        return averageTripRating;
    }

    public void setAverageTripRating(final Long averageTripRating) {
        this.averageTripRating = averageTripRating;
    }

    public Long getAverageDriverRating() {
        return averageDriverRating;
    }

    public void setAverageDriverRating(final Long averageDriverRating) {
        this.averageDriverRating = averageDriverRating;
    }
}
