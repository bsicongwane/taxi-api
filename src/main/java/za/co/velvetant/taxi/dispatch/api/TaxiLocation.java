package za.co.velvetant.taxi.dispatch.api;

import za.co.velvetant.taxi.api.common.Location;

import java.util.Date;

public class TaxiLocation extends Location {

    private String registration;
    private Date date;

    public TaxiLocation() {
    }

    public TaxiLocation(final String registration, final Double latitude, final Double longitude) {

        this(registration, new Date(), latitude, longitude);
    }

    public TaxiLocation(final String registration, final Date date, final Double latitude, final Double longitude) {
        super(latitude, longitude);
        this.registration = registration;
        this.date = new Date(date.getTime());
    }

    public Date getDate() {
        Date date = null;
        if (this.date != null) {
            date = new Date(this.date.getTime());
        }

        return date;
    }

    public void setDate(final Date date) {
        this.date = new Date(date.getTime());
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(final String registration) {
        this.registration = registration;
    }

}
