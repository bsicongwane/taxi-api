package za.co.velvetant.taxi.dispatch.api;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.Base;

public class TaxiTracking extends Base {

    private Location location;
    private String registration;
    private String time;
    private String distance;

    // no arg
    public TaxiTracking() {

    }

    public TaxiTracking(final String registration, final Location location, final String time, final String distance) {
        super();
        this.registration = registration;
        this.location = location;
        this.time = time;
        this.distance = distance;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(final Location location) {
        this.location = location;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(final String registration) {
        this.registration = registration;
    }

    public String getTime() {
        return time;
    }

    public void setTime(final String time) {
        this.time = time;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(final String distance) {
        this.distance = distance;
    }

}
