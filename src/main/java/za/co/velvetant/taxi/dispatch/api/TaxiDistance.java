package za.co.velvetant.taxi.dispatch.api;

import java.util.Date;

public class TaxiDistance extends TaxiLocation {

    private Double distance;

    public TaxiDistance() {
    }

    public TaxiDistance(final String registration, final Date date, final Double latitude, final Double longitude, final Double distance) {
        super(registration, date, latitude, longitude);
        this.distance = distance;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(final Double distance) {
        this.distance = distance;
    }

}
