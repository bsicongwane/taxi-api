package za.co.velvetant.taxi.dispatch.api.driver;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import za.co.velvetant.taxi.engine.api.Base;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DriverShift extends Base {

    private String driverId;
    private String firstName;
    private String lastName;

    private String taxiRegistration;
    private String driverBoxVersion;

    @SuppressWarnings("unused")
    protected DriverShift() {
        // required by jackson
    }

    public DriverShift(final String driverId, final String taxiRegistration) {
        this.driverId = driverId;
        this.taxiRegistration = taxiRegistration;
    }

    public DriverShift(final String driverId, final String taxiRegistration, final String driverBoxVersion) {

        this(driverId, taxiRegistration);
        this.driverBoxVersion = driverBoxVersion;
    }

    public DriverShift(final String driverId, final String firstName, final String lastName, final String taxiRegistration) {

        this(driverId, taxiRegistration);

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getTaxiRegistration() {
        return taxiRegistration;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDriverBoxVersion() {
        return driverBoxVersion;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof DriverShift)) {
            return false;
        }

        DriverShift shift = (DriverShift) obj;
        return new EqualsBuilder().append(shift.getDriverId(), driverId).append(shift.getTaxiRegistration(), taxiRegistration).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(driverId).append(taxiRegistration).hashCode();
    }
}
