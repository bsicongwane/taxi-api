package za.co.velvetant.taxi.dispatch.api.driver;

import java.util.Date;

import za.co.velvetant.taxi.api.common.Taxi;
import za.co.velvetant.taxi.engine.api.Base;
import za.co.velvetant.taxi.engine.api.Driver;

public class FullDriverShift extends Base {

    private String shiftId;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private Date started;
    private Date ended;
    private String state;

    private Driver driver;
    private Taxi taxi;

    @SuppressWarnings("unused")
    private FullDriverShift() {
        // required by jackson
    }

    public FullDriverShift(final String shiftId, final String userId, final String firstName, final String lastName, final String email, final Date started, final Date ended, final String state, final Driver driver, final Taxi taxi) {
        this.shiftId = shiftId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.started = started;
        this.ended = ended;
        this.state = state;
        this.driver = driver;
        this.taxi = taxi;
    }

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getStarted() {
        return started;
    }

    public Date getEnded() {
        return ended;
    }

    public String getState() {
        return state;
    }

    public Driver getDriver() {
        return driver;
    }

    public Taxi getTaxi() {
        return taxi;
    }

    public String getShiftId() {
        return shiftId;
    }

    public String getEmail() {
        return email;
    }
}
