package za.co.velvetant.taxi.ops.notifications;

public class FareAmountCapturedNotification extends SimpleNotification<FarePayment> {

    public FareAmountCapturedNotification() {
    }

    public FareAmountCapturedNotification(final String identifier, final String source, final String driver, final String paymentMethod, final Double paymentAmount) {
        super(identifier, source, new FarePayment(driver, paymentMethod, paymentAmount));
    }

    public FareAmountCapturedNotification(final String identifier, final Class source, final String driver, final String paymentMethod, final Double paymentAmount) {
        super(identifier, source, new FarePayment(driver, paymentMethod, paymentAmount));
    }

}
