package za.co.velvetant.taxi.ops.notifications;

import static za.co.velvetant.taxi.ops.notifications.Notification.Level.SYSTEM;

public abstract class SystemNotification<T> extends SimpleNotification<T> {

    protected SystemNotification() {
    }

    protected SystemNotification(final String identifier, final String source, final T entity) {
        super(identifier, SYSTEM, source, entity);
    }

    protected SystemNotification(final String identifier, final Class source, final T entity) {
        super(identifier, SYSTEM, source, entity);
    }
}
