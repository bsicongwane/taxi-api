package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.dispatch.api.driver.FullDriverShift;

public class DriverShiftStartedNotification extends SimpleNotification<FullDriverShift> {

    public DriverShiftStartedNotification() {
    }

    public DriverShiftStartedNotification(final String identifier, final String source, final FullDriverShift entity) {
        super(identifier, source, entity);
    }

    public DriverShiftStartedNotification(final String identifier, final Class source, final FullDriverShift entity) {
        super(identifier, source, entity);
    }
}
