package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.api.common.Location;

public class TaxiLocationUpdateNotification extends TrackingNotification<Location> {

    public TaxiLocationUpdateNotification() {
    }

    public TaxiLocationUpdateNotification(final String identifier, final String source, final Location entity) {
        super(identifier, source, entity);
    }

    public TaxiLocationUpdateNotification(final String identifier, final Class source, final Location entity) {
        super(identifier, source, entity);
    }
}
