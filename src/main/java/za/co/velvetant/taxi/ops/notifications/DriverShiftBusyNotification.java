package za.co.velvetant.taxi.ops.notifications;

public class DriverShiftBusyNotification extends SimpleNotification {

    public DriverShiftBusyNotification() {
    }

    public DriverShiftBusyNotification(final String identifier, final String source) {
        super(identifier, source, null);
    }

    public DriverShiftBusyNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }
}
