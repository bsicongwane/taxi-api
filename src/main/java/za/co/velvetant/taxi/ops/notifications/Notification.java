package za.co.velvetant.taxi.ops.notifications;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public interface Notification<T> {

    public enum Level {

        ACTION, TRACKING, INFO, SYSTEM
    }

    String getIdentifier();

    Level getLevel();

    String getSource();

    Date getTime();

    T getEntity();

    String getType();
}
