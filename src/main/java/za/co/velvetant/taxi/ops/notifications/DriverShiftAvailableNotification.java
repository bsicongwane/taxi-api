package za.co.velvetant.taxi.ops.notifications;

public class DriverShiftAvailableNotification extends SimpleNotification {

    public DriverShiftAvailableNotification() {
    }

    public DriverShiftAvailableNotification(final String identifier, final String source) {
        super(identifier, source, null);
    }

    public DriverShiftAvailableNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }
}
