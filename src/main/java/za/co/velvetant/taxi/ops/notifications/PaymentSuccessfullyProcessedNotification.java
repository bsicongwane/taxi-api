package za.co.velvetant.taxi.ops.notifications;

public class PaymentSuccessfullyProcessedNotification extends SimpleNotification<String> {

    public PaymentSuccessfullyProcessedNotification() {
    }

    public PaymentSuccessfullyProcessedNotification(final String identifier, final String source, final String entity) {
        super(identifier, source, entity);
    }

    public PaymentSuccessfullyProcessedNotification(final String identifier, final Class source, final String entity) {
        super(identifier, source, entity);
    }
}
