package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.engine.api.FareState;

public class FareCancelledNotification extends ActionNotification<FareState> {

    public FareCancelledNotification() {
    }

    public FareCancelledNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public FareCancelledNotification(final String identifier, final String source, final FareState entity) {
        super(identifier, source, entity);
    }

    public FareCancelledNotification(final String identifier, final Class source, final FareState entity) {
        super(identifier, source, entity);
    }
}
