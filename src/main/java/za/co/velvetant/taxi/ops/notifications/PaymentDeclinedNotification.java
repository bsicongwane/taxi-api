package za.co.velvetant.taxi.ops.notifications;

public class PaymentDeclinedNotification extends ActionNotification<AccountTransaction> {

    public PaymentDeclinedNotification() {
    }

    public PaymentDeclinedNotification(final String identifier, final String source, final AccountTransaction entity) {
        super(identifier, source, entity);
    }

    public PaymentDeclinedNotification(final String identifier, final Class source, final AccountTransaction entity) {
        super(identifier, source, entity);
    }
}
