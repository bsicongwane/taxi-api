package za.co.velvetant.taxi.ops.notifications;

import static za.co.velvetant.taxi.ops.notifications.Notification.Level.TRACKING;

public abstract class TrackingNotification<T> extends SimpleNotification<T> {

    protected TrackingNotification() {
    }

    protected TrackingNotification(final String identifier, final String source, final T entity) {
        super(identifier, TRACKING, source, entity);
    }

    protected TrackingNotification(final String identifier, final Class source, final T entity) {
        super(identifier, TRACKING, source, entity);
    }
}
