package za.co.velvetant.taxi.ops.notifications;

import java.util.Map;

import za.co.velvetant.taxi.api.common.TaxiDistance;
import za.co.velvetant.taxi.engine.api.Fare;

public class DispatchedFare {

    private Fare fare;
    private Map<String, TaxiDistance> drivers;
    private String window;

    public DispatchedFare() {
    }

    public DispatchedFare(final Fare fare, final Map<String, TaxiDistance> drivers, final String window) {
        this.fare = fare;
        this.drivers = drivers;
        this.window = window;
    }

    public Fare getFare() {
        return fare;
    }

    public Map<String, TaxiDistance> getDrivers() {
        return drivers;
    }

    public String getWindow() {
        return window;
    }
}
