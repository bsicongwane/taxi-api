package za.co.velvetant.taxi.ops.notifications;

public class PaymentConfirmationTimedOutNotification extends SimpleNotification {

    public PaymentConfirmationTimedOutNotification() {
    }

    public PaymentConfirmationTimedOutNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public PaymentConfirmationTimedOutNotification(final String identifier, final String source, final Object entity) {
        super(identifier, source, entity);
    }

    public PaymentConfirmationTimedOutNotification(final String identifier, final Class source, final Object entity) {
        super(identifier, source, entity);
    }
}
