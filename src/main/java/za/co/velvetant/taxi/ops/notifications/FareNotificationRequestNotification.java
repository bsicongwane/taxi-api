package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.driverbox.api.fare.FareNotification;

import java.util.Collection;

public class FareNotificationRequestNotification extends SimpleNotification<Collection<FareNotification>> {

    public FareNotificationRequestNotification() {
    }

    public FareNotificationRequestNotification(final String identifier, final String source, final Collection<FareNotification> entity) {
        super(identifier, source, entity);
    }

    public FareNotificationRequestNotification(final String identifier, final Class source, final Collection<FareNotification> entity) {
        super(identifier, source, entity);
    }
}
