package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.engine.api.DriverShift;

public class DriverAssignedNotification extends SimpleNotification<DriverShift> {

    public DriverAssignedNotification() {
    }

    public DriverAssignedNotification(final String identifier, final String source, final DriverShift entity) {
        super(identifier, source, entity);
    }

    public DriverAssignedNotification(final String identifier, final Class source, final DriverShift entity) {
        super(identifier, source, entity);
    }
}
