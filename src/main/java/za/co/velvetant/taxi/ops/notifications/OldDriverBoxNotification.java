package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.dispatch.api.driver.DriverShift;

public class OldDriverBoxNotification extends ActionNotification<DriverShift> {

    public OldDriverBoxNotification() {
    }

    public OldDriverBoxNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public OldDriverBoxNotification(final String identifier, final String source, final DriverShift entity) {
        super(identifier, source, entity);
    }

    public OldDriverBoxNotification(final String identifier, final Class source, final DriverShift entity) {
        super(identifier, source, entity);
    }
}
