package za.co.velvetant.taxi.ops.notifications;

public class PaymentFailedNotification extends ActionNotification<AccountTransaction> {

    public PaymentFailedNotification() {
    }

    public PaymentFailedNotification(final String identifier, final String source, final AccountTransaction entity) {
        super(identifier, source, entity);
    }

    public PaymentFailedNotification(final String identifier, final Class source, final AccountTransaction entity) {
        super(identifier, source, entity);
    }
}
