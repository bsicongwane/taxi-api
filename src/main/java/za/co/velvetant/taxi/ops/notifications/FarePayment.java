package za.co.velvetant.taxi.ops.notifications;

public class FarePayment {

    private String driver;
    private String paymentMethod;
    private Double paymentAmount;

    public FarePayment() {
    }

    public FarePayment(final String driver, final String paymentMethod, final Double paymentAmount) {
        this.driver = driver;
        this.paymentMethod = paymentMethod;
        this.paymentAmount = paymentAmount;
    }

    public String getDriver() {
        return driver;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }
}
