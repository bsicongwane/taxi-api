package za.co.velvetant.taxi.ops.notifications;

import static za.co.velvetant.taxi.ops.notifications.Notification.Level.ACTION;

public abstract class ActionNotification<T> extends SimpleNotification<T> {

    protected ActionNotification() {
    }

    protected ActionNotification(final String identifier, final String source, final T entity) {
        super(identifier, ACTION, source, entity);
    }

    protected ActionNotification(final String identifier, final Class source, final T entity) {
        super(identifier, ACTION, source, entity);
    }
}
