package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.engine.api.FareRequest;

public class NewFareRequestNotification extends SimpleNotification<FareRequest> {

    public NewFareRequestNotification() {
    }

    public NewFareRequestNotification(final String uuid, final String source, final FareRequest request) {
        super(uuid, source, request);
    }

    public NewFareRequestNotification(final String uuid, final Class source, final FareRequest request) {
        super(uuid, source, request);
    }
}
