package za.co.velvetant.taxi.ops.notifications;

public class DriverArrivedAtPickupNotification extends SimpleNotification<String> {

    public DriverArrivedAtPickupNotification() {
    }

    public DriverArrivedAtPickupNotification(final String identifier, final String source, final String entity) {
        super(identifier, source, entity);
    }

    public DriverArrivedAtPickupNotification(final String identifier, final Class source, final String entity) {
        super(identifier, source, entity);
    }
}
