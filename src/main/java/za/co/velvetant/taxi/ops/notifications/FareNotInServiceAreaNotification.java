package za.co.velvetant.taxi.ops.notifications;

public class FareNotInServiceAreaNotification extends SimpleNotification {

    public FareNotInServiceAreaNotification() {
    }

    public FareNotInServiceAreaNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public FareNotInServiceAreaNotification(final String identifier, final String source, final Object entity) {
        super(identifier, source, entity);
    }

    public FareNotInServiceAreaNotification(final String identifier, final Class source, final Object entity) {
        super(identifier, source, entity);
    }
}
