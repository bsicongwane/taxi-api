package za.co.velvetant.taxi.ops.notifications;

public class DriverShiftEndedNotification extends SimpleNotification {

    public DriverShiftEndedNotification() {
    }

    public DriverShiftEndedNotification(final String identifier, final String source) {
        super(identifier, source, null);
    }

    public DriverShiftEndedNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }
}
