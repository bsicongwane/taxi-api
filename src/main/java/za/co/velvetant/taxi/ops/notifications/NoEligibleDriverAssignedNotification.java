package za.co.velvetant.taxi.ops.notifications;

public class NoEligibleDriverAssignedNotification extends SimpleNotification {

    public NoEligibleDriverAssignedNotification() {
    }

    public NoEligibleDriverAssignedNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public NoEligibleDriverAssignedNotification(final String identifier, final String source, final Object entity) {
        super(identifier, source, entity);
    }

    public NoEligibleDriverAssignedNotification(final String identifier, final Class source, final Object entity) {
        super(identifier, source, entity);
    }
}
