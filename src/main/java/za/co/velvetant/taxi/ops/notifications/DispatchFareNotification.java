package za.co.velvetant.taxi.ops.notifications;

public class DispatchFareNotification extends SimpleNotification<DispatchedFare> {

    public DispatchFareNotification() {
    }

    public DispatchFareNotification(final String identifier, final String source, final DispatchedFare entity) {
        super(identifier, source, entity);
    }

    public DispatchFareNotification(final String identifier, final Class source, final DispatchedFare entity) {
        super(identifier, source, entity);
    }

}
