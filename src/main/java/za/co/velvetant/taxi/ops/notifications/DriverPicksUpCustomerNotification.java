package za.co.velvetant.taxi.ops.notifications;

public class DriverPicksUpCustomerNotification extends SimpleNotification<String> {

    public DriverPicksUpCustomerNotification() {
    }

    public DriverPicksUpCustomerNotification(final String identifier, final String source, final String entity) {
        super(identifier, source, entity);
    }

    public DriverPicksUpCustomerNotification(final String identifier, final Class source, final String entity) {
        super(identifier, source, entity);
    }
}
