package za.co.velvetant.taxi.ops.notifications;

public class FareRemovedNotification extends SimpleNotification {

    public FareRemovedNotification() {
    }

    public FareRemovedNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public FareRemovedNotification(final String identifier, final String source, final Object entity) {
        super(identifier, source, entity);
    }

    public FareRemovedNotification(final String identifier, final Class source, final Object entity) {
        super(identifier, source, entity);
    }
}
