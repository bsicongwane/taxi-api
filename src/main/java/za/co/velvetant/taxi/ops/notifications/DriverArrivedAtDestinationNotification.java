package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.api.common.DriverLocation;

public class DriverArrivedAtDestinationNotification extends SimpleNotification<DriverLocation> {

    public DriverArrivedAtDestinationNotification() {
    }

    public DriverArrivedAtDestinationNotification(final String identifier, final String source, final DriverLocation entity) {
        super(identifier, source, entity);
    }

    public DriverArrivedAtDestinationNotification(final String identifier, final Class source, final DriverLocation entity) {
        super(identifier, source, entity);
    }
}
