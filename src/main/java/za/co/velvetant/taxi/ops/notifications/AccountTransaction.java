package za.co.velvetant.taxi.ops.notifications;

import java.util.Date;

import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.PaymentMethod;

public class AccountTransaction {

    private Date transactionTime;
    private String transactionIndex;
    private Integer status;
    private String errorMessage;
    private String authorisationId;
    private Double amount;
    private Double tip;

    private CreditCard creditCard;
    private PaymentMethod paymentMethod;

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(final Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionIndex() {
        return transactionIndex;
    }

    public void setTransactionIndex(final String transactionIndex) {
        this.transactionIndex = transactionIndex;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(final Integer status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getAuthorisationId() {
        return authorisationId;
    }

    public void setAuthorisationId(final String authorisationId) {
        this.authorisationId = authorisationId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(final Double amount) {
        this.amount = amount;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(final Double tip) {
        this.tip = tip;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(final CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(final PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
