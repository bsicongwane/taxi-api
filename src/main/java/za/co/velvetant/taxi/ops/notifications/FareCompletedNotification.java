package za.co.velvetant.taxi.ops.notifications;

public class FareCompletedNotification extends SimpleNotification {

    public FareCompletedNotification() {
    }

    public FareCompletedNotification(final String identifier, final Class source) {
        super(identifier, source, null);
    }

    public FareCompletedNotification(final String identifier, final String source, final Object entity) {
        super(identifier, source, entity);
    }

    public FareCompletedNotification(final String identifier, final Class source, final Object entity) {
        super(identifier, source, entity);
    }
}
