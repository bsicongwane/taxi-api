package za.co.velvetant.taxi.ops.notifications;

import static za.co.velvetant.taxi.ops.notifications.Notification.Level.INFO;

import java.util.Date;

public abstract class SimpleNotification<T> implements Notification<T> {

    private String identifier;
    private Level level = INFO;
    private String source;
    private String type;
    private Date time = new Date();

    private T entity;

    public SimpleNotification() {
    }

    public SimpleNotification(final String identifier, final String source, final T entity) {
        this.identifier = identifier;
        this.source = source;
        this.entity = entity;

        this.type = this.getClass().getSimpleName();
    }

    public SimpleNotification(final String identifier, final Level level, final String source, final T entity) {
        this.identifier = identifier;
        this.level = level;
        this.source = source;
        this.entity = entity;

        this.type = this.getClass().getSimpleName();
    }

    public SimpleNotification(final String identifier, final Level level, final Class source, final T entity) {
        this(identifier, level, source.getName(), entity);
    }

    public SimpleNotification(final String identifier, final Class source, final T entity) {
        this(identifier, source.getName(), entity);
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public Level getLevel() {
        return level;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public T getEntity() {
        return entity;
    }

    @Override
    public String getType() {
        return type;
    }

    public void setTime(final Date time) {
        this.time = time;
    }

}
