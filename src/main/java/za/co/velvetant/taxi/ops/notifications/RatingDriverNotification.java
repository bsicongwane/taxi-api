package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.engine.api.Rating;

public class RatingDriverNotification extends SimpleNotification<Rating> {

    public RatingDriverNotification() {
    }

    public RatingDriverNotification(final String identifier, final String source, final Rating entity) {
        super(identifier, source, entity);
    }

    public RatingDriverNotification(final String identifier, final Class source, final Rating entity) {
        super(identifier, source, entity);
    }
}
