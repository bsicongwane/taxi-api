package za.co.velvetant.taxi.ops.notifications;

import za.co.velvetant.taxi.driverbox.api.fare.Customer;

public class CallCustomerRequestNotification extends ActionNotification<Customer> {

    public CallCustomerRequestNotification() {
    }

    public CallCustomerRequestNotification(final String identifier, final String source, final Customer entity) {
        super(identifier, source, entity);
    }

    public CallCustomerRequestNotification(final String identifier, final Class source, final Customer entity) {
        super(identifier, source, entity);
    }
}
