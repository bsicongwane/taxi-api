package za.co.velvetant.taxi.driverbox.api.fare;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This hack is only meant for release 1.2. It allows older Driver Box's to work with that release.
 * THIS SHOULD BE REVERTED ONCE 1.3 GOES OUT AND ALL DRIVERS HAVE UPGRADED THEIR DRIVER BOXES TO VERSION 1.2
 */
@Deprecated
public class FareCustomer extends Customer {

    private String cellphone;

    public FareCustomer(final String firstName, final String lastName, final String userId, final String contactNo) {
        super(firstName, lastName, userId, contactNo);
    }

    @Override
    @JsonIgnore
    public String getContactNo() {
        return super.getContactNo();
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(final String cellphone) {
        this.cellphone = cellphone;
    }
}
