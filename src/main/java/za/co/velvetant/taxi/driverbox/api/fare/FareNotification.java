package za.co.velvetant.taxi.driverbox.api.fare;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.PaymentMethod;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FareNotification {

    private String uuid;

    private Location pickUp;
    private Location dropOff;
    private Customer customer;
    private PaymentMethod paymentMethod;
    private String estimatedTripDistance;
    private String estimatedTripDuration;
    private String reference;
    private List<String> affiliates;
    private Integer window;

    @SuppressWarnings("unused")
    private FareNotification() {
    }

    public FareNotification(final String uuid, final String reference, final Customer customer, final PaymentMethod paymentMethod, final Location pickUp, final Location dropOff,
                            final String estimatedTripDistance, final String estimatedTripDuration, final List<String> affiliates, final Integer window) {
        this.uuid = uuid;
        this.reference = reference;
        this.pickUp = pickUp;
        this.dropOff = dropOff;
        this.customer = customer;
        this.paymentMethod = paymentMethod;
        this.estimatedTripDistance = estimatedTripDistance;
        this.estimatedTripDuration = estimatedTripDuration;
        this.affiliates = affiliates;
        this.window = window;
    }

    public String getUuid() {
        return uuid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public Location getPickUp() {
        return pickUp;
    }

    public Location getDropOff() {
        return dropOff;
    }

    public void setDropOff(Location dropOff) {

        this.dropOff = dropOff;
    }

    public String getEstimatedTripDistance() {
        return estimatedTripDistance;
    }

    public String getEstimatedTripDuration() {
        return estimatedTripDuration;
    }

    public boolean hasDropOff() {

        return dropOff != null;
    }

    public String getDropOffAddress() {

        return dropOff != null ? dropOff.getAddress() : null;
    }

    public String getReference() {
        return reference;
    }

    public String getPickUpAddress() {

        return getPickUp().getAddress();
    }

    public List<String> getAffiliates() {
        return affiliates;
    }

    public void setAffiliates(final List<String> affiliates) {
        this.affiliates = affiliates;
    }

    public Integer getWindow() {
        return window;
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
