package za.co.velvetant.taxi.driverbox.api.location;

@Deprecated
public class LocationWithAddress extends Location {

    private String address;

    public LocationWithAddress(final Double latitude, final Double longitude, final String address) {
        super(latitude, longitude);
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
