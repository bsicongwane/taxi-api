package za.co.velvetant.taxi.driverbox.api.fare;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Customer {

    private String firstName;
    private String lastName;
    private String userId;
    private String contactNo;

    @SuppressWarnings("unused")
    private Customer() {
    }

    public Customer(final String firstName, final String lastName, final String userId, final String contactNo) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.contactNo = contactNo;
    }

    public String getFirstName() {

        return firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public String getUserId(){

        return userId;
    }

    public String getContactNo() {

        return contactNo;
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
