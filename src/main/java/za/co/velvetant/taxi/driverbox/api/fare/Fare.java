package za.co.velvetant.taxi.driverbox.api.fare;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.FareState;
import za.co.velvetant.taxi.engine.api.PaymentMethod;

/**
 * This hack is only meant for release 1.2. It allows older Driver Box's to work with that release.
 * THIS SHOULD BE REVERTED ONCE 1.3 GOES OUT AND ALL DRIVERS HAVE UPGRADED THEIR DRIVER BOXES TO VERSION 1.2
 */
@Deprecated
public class Fare {

    private String uuid;
    private FareState state;
    private String reference;
    private FareCustomer customer;
    private PaymentMethod paymentMethod;
    private Location pickup;
    private Location dropOff;
    private String distance;
    private String duration;

    public Fare(final String uuid, final FareState state, final String reference, final FareCustomer customer, final PaymentMethod paymentMethod, final Location pickup, final Location dropOff,
            final String distance, final String duration) {
        this.uuid = uuid;
        this.state = state;
        this.reference = reference;
        this.customer = customer;
        this.paymentMethod = paymentMethod;
        this.pickup = pickup;
        this.dropOff = dropOff;
        this.distance = distance;
        this.duration = duration;
    }

    public String getUuid() {
        return uuid;
    }

    public FareState getState() {
        return state;
    }

    public String getReference() {
        return reference;
    }

    public FareCustomer getCustomer() {
        return customer;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public Location getPickup() {
        return pickup;
    }

    public Location getDropOff() {
        return dropOff;
    }

    public String getDistance() {
        return distance;
    }

    public String getDuration() {
        return duration;
    }
}
