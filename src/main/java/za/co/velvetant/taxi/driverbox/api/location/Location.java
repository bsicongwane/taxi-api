package za.co.velvetant.taxi.driverbox.api.location;

import java.io.Serializable;

@Deprecated
public class Location implements Serializable {

    private Double latitude;
    private Double longitude;

    @SuppressWarnings("unused")
    private Location() {
    }

    public Location(final Double latitude, final Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {

        return latitude;
    }

    public Double getLongitude() {

        return longitude;
    }
}
