package za.co.velvetant.taxi.api.common;

import java.util.Comparator;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import za.co.velvetant.taxi.engine.api.Base;

public class Location extends Base {

    public static class AddressComparator implements Comparator<Location> {

        @Override
        public int compare(final Location o1, final Location o2) {
            if (o1 == null || o2 == null) {
                return 0;
            }
            if (o1.getAddress() != null && o2.getAddress() != null) {
                return o1.getAddress().compareTo(o2.getAddress());
            }
            return 0;
        }

    }

    private Double longitude;
    private Double latitude;

    private String address;
    private Float speed;

    public Location() {
    }

    public Location(final Double latitude, final Double longitude) {

        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Location(final Double latitude, final Double longitude, final String address) {

        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
    }

    public Location(final Double latitude, final Double longitude, final Float speed) {

        this(latitude, longitude);
        this.speed = speed;
    }

    public Number getLongitude() {
        return longitude;
    }

    public Number getLatitude() {
        return latitude;
    }

    public Float getSpeed() {
        return speed;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
