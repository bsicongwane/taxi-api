package za.co.velvetant.taxi.api.common;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Taxi {

    private String registration;

    @SuppressWarnings("unused")
    private Taxi() {
    }

    public Taxi(final String registration) {
        this.registration = registration;
    }

    public String getRegistration() {
        return registration;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
