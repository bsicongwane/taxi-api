package za.co.velvetant.taxi.api.common;

import za.co.velvetant.taxi.engine.api.Driver;

public class DriverLocation {

    private Driver driver;
    private Location location;

    @SuppressWarnings("unused")
    private DriverLocation() {
    }

    public DriverLocation(final Driver driver, final Location location) {
        this.driver = driver;
        this.location = location;
    }

    public DriverLocation(final Driver driver, final Double latitude, final Double longitude, final Float speed) {

        this.location = new Location(latitude, longitude, speed);
        this.driver = driver;
    }

    public Location getLocation() {
        return location;
    }

    public Driver getDriver() {
        return driver;
    }
}
