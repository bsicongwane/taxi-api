package za.co.velvetant.taxi.api.common;

import za.co.velvetant.taxi.engine.api.Base;

public class AffiliateLocation extends Base {

    private Long radius;
    private Location location;

    @SuppressWarnings("unused")
    private AffiliateLocation() {
    }

    public AffiliateLocation(final Double latitude, final Double longitude, final Long radius) {

        this.location = new Location(latitude, longitude);
        this.radius = radius;
    }
    
    public AffiliateLocation(final Double latitude, final Double longitude, final String address, final Long radius) {

        this.location =  new Location(latitude, longitude, address);
        this.radius = radius;
    }

    public Location getLocation() {
        return location;
    }

    public Long getRadius() {
        return radius;
    }

    public void setRadius(Long radius) {
        this.radius = radius;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
