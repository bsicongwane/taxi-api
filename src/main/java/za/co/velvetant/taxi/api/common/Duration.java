package za.co.velvetant.taxi.api.common;

public class Duration {

    private String text;
    private long value;

    public Duration() { }

    public Duration(final String text, final long value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public long getValue() {
        return value;
    }

    public void setValue(final long value) {
        this.value = value;
    }

}
