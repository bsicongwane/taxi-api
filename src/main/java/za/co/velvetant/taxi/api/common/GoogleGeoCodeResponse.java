package za.co.velvetant.taxi.api.common;

public class GoogleGeoCodeResponse {

    public static class Results {

        private String formatted_address;

        private Geometry geometry;

        private String[] types;

        private AddressComponent[] address_components;

        public String getFormatted_address() {
            return formatted_address;
        }

        public String[] getTypes() {
            return types;
        }

    }

    private String status;

    private Results[] results;

    public Results[] getResults() {
        return results;
    }

}

class Geometry {

    class Location {

        private String lat;

        private String lng;
    }

    private String location_type;

    private Geometry.Location location;
}

class AddressComponent {

    private String long_name;

    private String short_name;

    private String[] types;
}
