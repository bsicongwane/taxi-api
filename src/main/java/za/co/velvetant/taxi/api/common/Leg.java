package za.co.velvetant.taxi.api.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Leg {

    private Distance distance;
    private Duration duration;
    @JsonProperty("start_address")
    private String startAddress;
    @JsonProperty("end_address")
    private String endAddress;
    @JsonProperty("start_location")
    private GoogleLocation startLocation;
    @JsonProperty("end_location")
    private GoogleLocation endLocation;


    public Leg() {
    }

    public Leg(final Distance distance, final Duration duration, final String startAddress, final String endAddress) {
        this.distance = distance;
        this.duration = duration;
        this.startAddress = startAddress;
        this.endAddress = endAddress;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(final Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(final Duration duration) {
        this.duration = duration;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(final String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(final String endAddress) {
        this.endAddress = endAddress;
    }

    public GoogleLocation getStartLocation() {
        return startLocation;
    }

    public GoogleLocation getEndLocation() {
        return endLocation;
    }
}
