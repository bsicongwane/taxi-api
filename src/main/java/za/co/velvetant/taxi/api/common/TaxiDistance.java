package za.co.velvetant.taxi.api.common;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TaxiDistance {

    private Taxi taxi;
    private Number distance;

    @SuppressWarnings("UnusedDeclaration")
    TaxiDistance() {
    }

    public TaxiDistance(final Taxi taxi, final Number distance) {

        this.taxi = taxi;
        this.distance = distance;
    }

    public Taxi getTaxi() {

        return taxi;
    }

    public Number getDistance() {

        return distance;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
