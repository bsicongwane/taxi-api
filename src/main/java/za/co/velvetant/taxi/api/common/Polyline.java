package za.co.velvetant.taxi.api.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Polyline{

    private String points;

    public String getPoints() {
        return points;
    }
}
