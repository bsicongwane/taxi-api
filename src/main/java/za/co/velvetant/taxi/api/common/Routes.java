package za.co.velvetant.taxi.api.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Routes {

    private List<Leg> legs = new ArrayList<Leg>();
    @JsonProperty("overview_polyline")
    private Polyline overviewPolyline;

    public Polyline getOverviewPolyline() {

        return overviewPolyline;
    }

    public Leg getFirstLeg() {
        Leg leg = null;
        if (legs.size() > 0) {
            leg = legs.get(0);
        }

        return leg;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(final List<Leg> legs) {
        this.legs = legs;
    }

}
