package za.co.velvetant.taxi.api.common;

public class TaxiLocation {

    private Taxi taxi;
    private Location location;

    @SuppressWarnings("unused")
    private TaxiLocation() {
    }

    public TaxiLocation(final Taxi taxi, final Location location) {
        this.taxi = taxi;
        this.location = location;
    }

    public TaxiLocation(final Taxi taxi, final Double latitude, final Double longitude, final Float speed) {

        this.location = new Location(latitude, longitude, speed);
        this.taxi = taxi;
    }

    public Location getLocation() {
        return location;
    }

    public Taxi getTaxi() {
        return taxi;
    }
}
