package za.co.velvetant.taxi.api.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Directions {

    private List<Routes> routes = new ArrayList<Routes>();

    public Routes getFirstRoute() {
        Routes route = null;
        if (this.routes.size() > 0) {
            route = routes.get(0);
        }

        return route;
    }

    public List<Routes> getRoutes() {
        return routes;
    }

    public void setRoutes(final List<Routes> routes) {
        this.routes = routes;
    }
}
