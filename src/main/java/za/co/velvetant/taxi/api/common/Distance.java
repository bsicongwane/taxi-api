package za.co.velvetant.taxi.api.common;

public class Distance {

    private String text;
    private long value;

    public Distance() {
    }

    public Distance(final String text, final long value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public long getValue() {
        return value;
    }

    public void setValue(final long value) {
        this.value = value;
    }
}
