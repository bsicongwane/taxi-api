package za.co.velvetant.taxi.engine.api;

public class Operations {

    public enum Action {
        NO_OPERATOR_ACCEPTED
    }

    private Action action;
    private Object data;

    public Operations() {
    }

    public Operations(final Action action, final Object data) {
        this.action = action;
        this.data = data;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(final Action action) {
        this.action = action;
    }

    public Object getData() {
        return data;
    }

    public void setData(final Object data) {
        this.data = data;
    }
}
