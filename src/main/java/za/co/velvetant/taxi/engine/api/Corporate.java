package za.co.velvetant.taxi.engine.api;

public class Corporate extends Company {

    private String sectorName;
    private String industryName;
    private String hash;

    public Corporate() {
    }

    public Corporate(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode, final String registration, final String vat, final Boolean taxExempt, final String sectorName,
            final String industryName) {
        super(companyName, billingAddress1, billingAddress2, billingAddress3, billingPostalCode, postalAddress1, postalAddress2, postalAddress3, postalPostalCode, registration, vat, taxExempt);
        this.sectorName = sectorName;
        this.industryName = industryName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public void setSectorName(final String sectorName) {
        this.sectorName = sectorName;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(final String industryName) {
        this.industryName = industryName;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

}
