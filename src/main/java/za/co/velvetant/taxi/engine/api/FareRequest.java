package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.List;

import za.co.velvetant.taxi.api.common.Location;

public class FareRequest extends Base {

    private Long pickupTime;
    private Location pickup;
    private Location dropOff;
    private String customer;

    private String paymentMethod;
    private List<String> affiliateIds = new ArrayList<String>();

    public FareRequest() {
    }

    public FareRequest(final Long pickupTime, final Location pickup, final Location dropOff, final String customer, final String paymentMethod) {
        this.pickupTime = pickupTime;
        this.pickup = pickup;
        this.dropOff = dropOff;
        this.customer = customer;
        this.paymentMethod = paymentMethod;
    }

    public FareRequest(final Long pickupTime, final Location pickup, final String customer, final String paymentMethod) {
        this(pickupTime, pickup, null, customer, paymentMethod);
    }

    public Long getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(final Long pickupTime) {
        this.pickupTime = pickupTime;
    }

    public Location getPickup() {
        return pickup;
    }

    public void setPickup(final Location pickup) {
        this.pickup = pickup;
    }

    public Location getDropOff() {
        return dropOff;
    }

    public void setDropOff(final Location dropOff) {
        this.dropOff = dropOff;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(final String customer) {
        this.customer = customer;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(final String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<String> getAffiliateIds() {
        return affiliateIds;
    }

    public void setAffiliateIds(final List<String> affiliateIds) {
        this.affiliateIds = affiliateIds;
    }

    public void addAffiliateId(final String affiliateId) {
        affiliateIds.add(affiliateId);
    }
}
