package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.List;

import za.co.velvetant.taxi.api.common.AffiliateLocation;

public class TaxiCompany extends Company {

    private int cabCount;
    private String administratorEmail;

    private List<AffiliateLocation> locations = new ArrayList<AffiliateLocation>();

    public TaxiCompany(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode, final String registration, final String vat, final Boolean taxExempt) {
        super(companyName, billingAddress1, billingAddress2, billingAddress3, billingPostalCode, postalAddress1, postalAddress2, postalAddress3, postalPostalCode, registration, vat, taxExempt);
    }

    public TaxiCompany() {
    }

    public int getCabCount() {
        return cabCount;
    }

    public void setCabCount(final int cabCount) {
        this.cabCount = cabCount;
    }

    public String getAdministratorEmail() {
        return administratorEmail;
    }

    public void setAdministratorEmail(final String administratorEmail) {
        this.administratorEmail = administratorEmail;
    }

    public List<AffiliateLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<AffiliateLocation> locations) {
        this.locations = locations;
    }

}
