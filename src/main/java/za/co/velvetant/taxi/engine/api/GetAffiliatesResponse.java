package za.co.velvetant.taxi.engine.api;

public class GetAffiliatesResponse extends Base {

    private String name;
    private int cabCount;
    private long timeStamp;
    private String registration;

    public GetAffiliatesResponse() {

    }

    public GetAffiliatesResponse(final String name, final int cabCount, final long timeStamp, final String registration) {
        super();
        this.name = name;
        this.cabCount = cabCount;
        this.timeStamp = timeStamp;
        this.registration = registration;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getCabCount() {
        return cabCount;
    }

    public void setCabCount(final int cabCount) {
        this.cabCount = cabCount;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(final long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(final String registration) {
        this.registration = registration;
    }

}
