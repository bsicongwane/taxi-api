package za.co.velvetant.taxi.engine.api;

public enum Role {

    CUSTOMER,
    DRIVER,
    CORPORATE,
    FAMILY,
    AGENT,
    SUPERVISOR,
    ADMINISTRATOR,
    SYSTEM;
}
