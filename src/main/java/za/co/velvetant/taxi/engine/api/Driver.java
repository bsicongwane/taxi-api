package za.co.velvetant.taxi.engine.api;

public class Driver extends Person {

    private String idNumber;
    private String status;
    private String rating;

    private TaxiCompany taxiCompany;

    public Driver() {
    }

    public Driver(final String firstName, final String lastName, final String cellphone, final String email, final String idNumber, final String userId) {
        super(firstName, lastName, cellphone, email, userId);
        this.idNumber = idNumber;
        this.status = "OFFLINE";
        setFirstName(firstName);
        setLastName(lastName);
    }

    public Driver(final String firstName, final String lastName, final String cellphone, final String email, final String idNumber, final String userId, final TaxiCompany taxiCompany) {
        super(firstName, lastName, cellphone, email, userId);
        this.idNumber = idNumber;
        this.status = "OFFLINE";
        this.taxiCompany = taxiCompany;
        setFirstName(firstName);
        setLastName(lastName);
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(final String idNumber) {
        this.idNumber = idNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(final String rating) {
        this.rating = rating;
    }

    public TaxiCompany getTaxiCompany() {
        return taxiCompany;
    }

    public void setTaxiCompany(TaxiCompany taxiCompany) {
        this.taxiCompany = taxiCompany;
    }
}
