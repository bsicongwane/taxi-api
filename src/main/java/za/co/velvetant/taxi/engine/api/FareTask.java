package za.co.velvetant.taxi.engine.api;

public class FareTask extends Base {

    private String uuid;
    private String status;

    public FareTask() {
    }

    public FareTask(final String uuid, final String status) {
        this.uuid = uuid;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

}
