package za.co.velvetant.taxi.engine.api;

public enum FareState {

    REQUESTED,
    WAIT_FOR_DRIVERS,
    ACCEPT_FARES,
    WAIT_FOR_DRIVER,
    PICK_UP_CUSTOMER,
    GET_PICKED_UP,
    ARRIVE_AT_PICKUP,
    ARRIVED_AT_DESTINATION,
    CAPTURE_FARE_AMOUNT,
    AWAIT_PAYMENT_CONFIRMATION,
    RATE_DRIVER,
    COMPLETED,
    CANCELLED_BY_DRIVER,
    CANCELLED_BY_CUSTOMER,
    CANCELLED_BY_ADMIN,
    CANCELLED,
    TIMED_OUT,
    OUT_OF_SERVICE_AREA;

    public boolean isCancelled() {
        return this.equals(CANCELLED) || this.equals(CANCELLED_BY_CUSTOMER) || this.equals(CANCELLED_BY_ADMIN) || this.equals(CANCELLED_BY_DRIVER) || this.equals(TIMED_OUT) || this.equals(OUT_OF_SERVICE_AREA);
    }
}
