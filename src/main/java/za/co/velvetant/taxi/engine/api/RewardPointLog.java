package za.co.velvetant.taxi.engine.api;

import za.co.velvetant.taxi.api.common.Location;

public class RewardPointLog extends Base {

    private long transactionTime;
    private Long amount;
    private Location from;
    private Location to;

    public long getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(final long transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(final Long amount) {
        this.amount = amount;
    }

    public Location getFrom() {
        return from;
    }

    public void setFrom(final Location from) {
        this.from = from;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(final Location to) {
        this.to = to;
    }

}
