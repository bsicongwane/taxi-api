package za.co.velvetant.taxi.engine.api;

import java.util.Date;

public class SystemProperty extends Base {

    private String name;
    private String value;
    private Date startTime;
    private Date endTime;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public Date getStartTime() {
        Date date = null;
        if (startTime != null) {
            date = new Date(startTime.getTime());
        }

        return date;
    }

    public void setStartTime(final Date startTime) {
        if (startTime != null) {
            this.startTime = new Date(startTime.getTime());
        }
    }

    public Date getEndTime() {
        Date date = null;
        if (endTime != null) {
            date = new Date(endTime.getTime());
        }

        return date;
    }

    public void setEndTime(final Date endTime) {
        if (endTime != null) {
            this.endTime = new Date(endTime.getTime());
        }
    }

    public void activate() {
        this.endTime = null;
    }

}
