package za.co.velvetant.taxi.engine.api;

import java.util.Comparator;

public class FareRequestTimeComparator implements Comparator<Fare> {

    @Override
    public int compare(final Fare o1, final Fare o2) {
        if (o1.getRequestTime() != null && o2.getRequestTime() != null) {
            return o1.getRequestTime().compareTo(o2.getRequestTime());

        }
        return 0;
    }
}

