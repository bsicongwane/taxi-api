package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.List;

public class User extends Person {

    private List<Role> userRoles = new ArrayList<Role>();

    public User() {
    }

    public User(final String firstName, final String lastName, final String cellphone, final String email) {
        super(firstName, lastName, cellphone, email, email);
    }

    public List<Role> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(final List<Role> userRoles) {
        this.userRoles = userRoles;
    }
}
