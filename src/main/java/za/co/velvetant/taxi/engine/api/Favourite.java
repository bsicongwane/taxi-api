package za.co.velvetant.taxi.engine.api;

import za.co.velvetant.taxi.api.common.Location;

public class Favourite extends Base {

    private String name;
    private Location location;

    public Favourite() {
    }

    public Favourite(final String name, final Location location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(final Location location) {
        this.location = location;
    }

}
