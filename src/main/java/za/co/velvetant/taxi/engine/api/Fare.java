package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import za.co.velvetant.taxi.api.common.Location;

public class Fare extends Base {

    private String uuid;
    private Customer customer;
    private Driver driver;
    private PaymentMethod paymentMethod;
    private Rating rating;
    private Date acceptedTime;
    private Date requestTime;
    private Date dropOffTime;
    private Date pickUpTime;
    private Double tip;
    private Double amount;
    private Double invoiceAmount;
    private String distance;
    private String duration;
    private Taxi taxi;
    private Location pickup;
    private Location dropOff;
    private String reference;
    private List<String> affiliates = new ArrayList<String>();
    private FareState state;
    private Long rewards;
    private String driverAffiliate;

    public Fare() {
    }

    public Fare(final Customer customer, final Date acceptedTime, final Location pickup, final Location dropOff) {
        this.customer = customer;
        this.pickup = pickup;
        this.dropOff = dropOff;

        if (acceptedTime != null) {
            this.acceptedTime = new Date(acceptedTime.getTime());
        }
    }

    public Fare(final String uuid,
                final Customer customer,
                final Driver driver,
                final PaymentMethod paymentMethod,
                final Rating rating,
                final Date acceptedTime,
                final Date requestTime,
                final Date dropOffTime,
                final Date pickUpTime,
                final Double amount,
                final Taxi taxi,
                final Location pickup,
                final Location dropOff,
                final String distance,
                final String duration) {
        this.uuid = uuid;
        this.customer = customer;
        this.driver = driver;
        this.paymentMethod = paymentMethod;
        this.rating = rating;
        this.amount = amount;
        this.taxi = taxi;
        this.pickup = pickup;
        this.dropOff = dropOff;
        this.distance = distance;
        this.duration = duration;

        if (acceptedTime != null) {
            this.acceptedTime = new Date(acceptedTime.getTime());
        }

        if (requestTime != null) {
            this.requestTime = new Date(requestTime.getTime());
        }

        if (dropOffTime != null) {
            this.dropOffTime = new Date(dropOffTime.getTime());
        }

        if (pickUpTime != null) {
            this.pickUpTime = new Date(pickUpTime.getTime());
        }
    }

    public String getUuid() {
        return uuid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Driver getDriver() {
        return driver;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public Rating getRating() {
        return rating;
    }

    public Date getAcceptedTime() {
        Date date = null;
        if (acceptedTime != null) {
            date = new Date(acceptedTime.getTime());
        }

        return date;
    }

    public Date getRequestTime() {
        Date date = null;
        if (requestTime != null) {
            date = new Date(requestTime.getTime());
        }

        return date;
    }

    public Date getDropOffTime() {
        Date date = null;
        if (dropOffTime != null) {
            date = new Date(dropOffTime.getTime());
        }

        return date;
    }

    public Date getPickUpTime() {
        Date date = null;
        if (pickUpTime != null) {
            date = new Date(pickUpTime.getTime());
        }

        return date;
    }

    public void setTip(final Double tip) {
        this.tip = tip;
    }

    public Taxi getTaxi() {
        return taxi;
    }

    public Location getPickup() {
        return pickup;
    }

    public Location getDropOff() {
        return dropOff;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public void setDriver(final Driver driver) {
        this.driver = driver;
    }

    public void setPaymentMethod(final PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setRating(final Rating rating) {
        this.rating = rating;
    }

    public void setAcceptedTime(final Date acceptedTime) {
        if (acceptedTime != null) {
            this.acceptedTime = new Date(acceptedTime.getTime());
        }
    }

    public void setRequestTime(final Date requestTime) {
        if (requestTime != null) {
            this.requestTime = new Date(requestTime.getTime());
        }
    }

    public void setDropOffTime(final Date dropOffTime) {
        if (dropOffTime != null) {
            this.dropOffTime = new Date(dropOffTime.getTime());
        }
    }

    public void setPickUpTime(final Date pickUpTime) {
        if (pickUpTime != null) {
            this.pickUpTime = new Date(pickUpTime.getTime());
        }
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(final Double amount) {
        this.amount = amount;
    }

    public Double getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(final Double invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public void setTaxi(final Taxi taxi) {
        this.taxi = taxi;
    }

    public void setPickup(final Location pickup) {
        this.pickup = pickup;
    }

    public void setDropOff(final Location dropOff) {
        this.dropOff = dropOff;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(final String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(final String duration) {
        this.duration = duration;
    }

    public Double getTip() {
        return tip;
    }

    public Long getRewards() {
        return rewards;
    }

    public void setRewards(final Long rewards) {
        this.rewards = rewards;
    }

    public String getReference() {
        return reference;
    }

    public List<String> getAffiliates() {
        return affiliates;
    }

    public void setAffiliates(final List<String> affiliates) {
        this.affiliates = affiliates;
    }

    public void addAffiliates(final List<String> affiliates) {
        if (this.affiliates == null) {
            this.affiliates = new ArrayList<String>();
        }

        this.affiliates.addAll(affiliates);
    }

    public FareState getState() {
        return state;
    }

    public void setState(final FareState state) {
        this.state = state;
    }

    public void setReference(final String reference) {
        this.reference = reference;
    }

    public String getDriverAffiliate() {
        return driverAffiliate;
    }

    public void setDriverAffiliate(final String driverAffiliate) {
        this.driverAffiliate = driverAffiliate;
    }
}
