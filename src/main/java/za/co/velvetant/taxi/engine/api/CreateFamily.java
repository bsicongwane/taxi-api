package za.co.velvetant.taxi.engine.api;

public class CreateFamily extends Base {

    private String familyName;

    public CreateFamily() {
    }

    public CreateFamily(final String familyName) {
        super();
        this.familyName = familyName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(final String familyName) {
        this.familyName = familyName;
    }

}
