package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.List;

public class Family extends Base {

    private String familyName;
    private String uuid;

    private List<String> administratorEmails = new ArrayList<String>();

    public Family() {
    }

    public Family(final String familyName, final String uuid) {
        super();
        this.familyName = familyName;
        this.uuid = uuid;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(final String familyName) {
        this.familyName = familyName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public List<String> getAdministratorEmails() {
        return administratorEmails;
    }

    public void setAdministratorEmails(final List<String> administratorEmails) {
        this.administratorEmails = administratorEmails;
    }

}
