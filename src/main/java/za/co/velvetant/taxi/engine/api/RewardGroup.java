package za.co.velvetant.taxi.engine.api;


public class RewardGroup extends Base {

    private Long amount;
    private String groupName;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(final Long amount) {
        this.amount = amount;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

}
