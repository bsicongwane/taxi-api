package za.co.velvetant.taxi.engine.api;

public class CreateCustomer extends Person {

    private Boolean sendNewsLetter;

    public CreateCustomer() {
    }

    public CreateCustomer(final String firstName, final String lastName, final String cellphone, final String email) {
        super(firstName, lastName, cellphone, email, email);
    }

    public CreateCustomer(final String firstName, final String lastName, final String cellphone, final String email, final String password) {
        this(firstName, lastName, cellphone, email);
        setPassword(password);
    }

    public Boolean getSendNewsLetter() {
        return sendNewsLetter;
    }

    public void setSendNewsLetter(final Boolean sendNewsLetter) {
        this.sendNewsLetter = sendNewsLetter;
    }
}
