package za.co.velvetant.taxi.engine.api;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class BaseId extends Base {

    private Long id;

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof BaseId)) {
            return false;
        }

        return new EqualsBuilder().append(id, ((BaseId) obj).getId()).isEquals();
    }
}
