package za.co.velvetant.taxi.engine.api;

import za.co.velvetant.taxi.api.common.Location;

public class EditFavourite extends Favourite {

    public EditFavourite() {
    }

    public EditFavourite(final String newName, final Location location) {
        setName(newName);
        setLocation(location);
    }

}
