package za.co.velvetant.taxi.engine.api;

import java.io.Serializable;

public class GsonTypeWrapper implements Serializable {

    private String className;
    private String data;

    public GsonTypeWrapper() {
    }

    public GsonTypeWrapper(final String className, final String data) {
        this.className = className;
        this.data = data;
    }

    public String getClassName() {
        return className;
    }

    public String getData() {
        return data;
    }

}
