package za.co.velvetant.taxi.engine.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("response")
public class SmsResponse implements Serializable {

    private Response response;

    public SmsResponse() {

    }

    public SmsResponse(final Message message) {
        this.response = new Response(message);

    }

    public Response getResponse() {
        return response;
    }


    public static class Message implements Serializable {

        private String to;
        private String id;
        private String status;
        private String credits;
        private String reference;
        private String content;

        public Message() {

        }

        public Message(final String to, final String reference, final String content) {
            this.to = to;
            this.reference = reference;
            this.content = content;
        }

        public String getTo() {
            return to;
        }

        public String getId() {
            return id;
        }

        public String getStatus() {
            return status;
        }

        public String getCredits() {
            return credits;
        }

        public String getReference() {
            return reference;
        }

        public String getContent() {
            return content;
        }

        @Override
        public String toString() {
            return "Message [to=" + to + ", id=" + id + ", status=" + status + ", credits=" + credits + ", reference=" + reference + ", content=" + content + "]";
        }

    }

    static class Response implements Serializable {

        private List<Message> messages;

        public Response() {

        }

        public Response(final Message message) {
            this.messages = new ArrayList<Message>();
            messages.add(message);
        }

        public List<Message> getMessages() {
            return messages;
        }

        @Override
        public String toString() {
            return "Response [messages=" + messages + "]";
        }

    }

    @Override
    public String toString() {
        return "SmsResponse [response=" + response + "]";
    }

}
