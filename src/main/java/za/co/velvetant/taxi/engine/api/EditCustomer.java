package za.co.velvetant.taxi.engine.api;

public class EditCustomer extends BaseId {

    private String firstName;
    private String lastName;
    private String cellphone;
    private Boolean sendNewsLetter;

    public EditCustomer() {
    }

    public EditCustomer(final String firstName, final String lastName, final String cellphone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cellphone = cellphone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public void setCellphone(final String cellphone) {
        this.cellphone = cellphone;
    }

    public Boolean getSendNewsLetter() {
        return sendNewsLetter;
    }

    public void setSendNewsLetter(final Boolean sendNewsLetter) {
        this.sendNewsLetter = sendNewsLetter;
    }

}
