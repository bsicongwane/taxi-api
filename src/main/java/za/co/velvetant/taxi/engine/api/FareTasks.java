package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.List;

public class FareTasks extends Base {

    private String uuid;
    private List<FareTask> tasks = new ArrayList<FareTask>();

    public FareTasks() {
    }

    public FareTasks(final String uuid) {
        this.uuid = uuid;
    }

    public FareTasks(final String uuid, final List<FareTask> tasks) {
        this.uuid = uuid;
        this.tasks = tasks;
    }

    public String getUuid() {
        return uuid;
    }

    public List<FareTask> getTasks() {
        return tasks;
    }

    public FareTask firstAction() {
        FareTask task = null;
        if (!tasks.isEmpty()) {
            task = tasks.get(0);
        }

        return task;
    }

    public boolean nextTask(String task){

        return firstAction().getStatus().equals(task);
    }

    public void setTasks(final List<FareTask> tasks) {
        this.tasks = tasks;
    }

    public void addTask(final FareTask action) {
        tasks.add(action);
    }

    public boolean haveTasks() {

        return !tasks.isEmpty();
    }
}
