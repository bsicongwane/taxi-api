package za.co.velvetant.taxi.engine.api.coupon;

import za.co.velvetant.taxi.engine.api.Base;

import java.math.BigDecimal;
import java.util.Date;

public class Coupon extends Base {

    private String uuid;
    private String couponNumber;
    private String description;
    private Date validFrom;
    private Date validTo;
    private BigDecimal monetaryLimit;
    private Integer usageLimit;
    private Integer usageCount;
    private Boolean active;

    public Coupon() {
    }

    public Coupon(final String uuid,
                  final String couponNumber,
                  final String description,
                  final Date validFrom,
                  final Date validTo,
                  final BigDecimal monetaryLimit,
                  final Integer usageLimit) {
        this.uuid = uuid;
        this.couponNumber = couponNumber;
        this.description = description;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.monetaryLimit = monetaryLimit;
        this.usageLimit = usageLimit;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public String getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(final Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(final Date validTo) {
        this.validTo = validTo;
    }

    public Integer getUsageLimit() {
        return usageLimit;
    }

    public void setUsageLimit(final Integer usageLimit) {
        this.usageLimit = usageLimit;
    }

    public BigDecimal getMonetaryLimit() {
        return monetaryLimit;
    }

    public void setMonetaryLimit(final BigDecimal monetaryLimit) {
        this.monetaryLimit = monetaryLimit;
    }

    public Integer getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(final Integer usageCount) {
        this.usageCount = usageCount;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }
}
