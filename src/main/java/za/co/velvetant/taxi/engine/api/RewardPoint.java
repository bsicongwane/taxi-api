package za.co.velvetant.taxi.engine.api;

public class RewardPoint extends Base {

    private Long total;
    private String rewardGroup;

    public RewardPoint() {

    }

    public RewardPoint(final Long total, final String rewardGroup) {
        super();
        this.total = total;
        this.rewardGroup = rewardGroup;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(final Long total) {
        this.total = total;
    }

    public String getRewardGroup() {
        return rewardGroup;
    }

    public void setRewardGroup(final String rewardGroup) {
        this.rewardGroup = rewardGroup;
    }

}
