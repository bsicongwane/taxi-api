package za.co.velvetant.taxi.engine.api;

public class Industry extends BaseId {

    private String name;

    public Industry() {
    }

    public Industry(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
