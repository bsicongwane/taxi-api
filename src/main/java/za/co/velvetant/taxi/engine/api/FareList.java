package za.co.velvetant.taxi.engine.api;

import java.util.List;

public class FareList {

    private List<Fare> fares;
    private int total;
    private Double totalAmount;

    public FareList(final List<Fare> fares, final int total, final Double totalAmount) {
        super();
        this.fares = fares;
        this.total = total;
        this.totalAmount = totalAmount;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(final List<Fare> fares) {
        this.fares = fares;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(final int total) {
        this.total = total;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(final Double totalAmount) {
        this.totalAmount = totalAmount;
    }

}
