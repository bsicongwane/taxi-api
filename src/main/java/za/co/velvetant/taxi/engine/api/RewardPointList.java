package za.co.velvetant.taxi.engine.api;

import java.util.HashMap;
import java.util.Map;

public class RewardPointList extends Base {

    private Map<String, Long> points = new HashMap<String, Long>();

    private Long total;

    public RewardPointList() {

    }

    public RewardPointList(final Map<String, Long> points, final Long total) {
        super();
        this.points = points;
        this.total = total;
    }

    public Map<String, Long> getPoints() {
        return points;
    }

    public void setPoints(final Map<String, Long> points) {
        this.points = points;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(final Long total) {
        this.total = total;
    }

}
