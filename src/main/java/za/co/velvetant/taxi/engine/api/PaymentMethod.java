package za.co.velvetant.taxi.engine.api;

public enum PaymentMethod {

    CASH, CREDIT_CARD, CORPORATE_ACCOUNT, FAMILY_ACCOUNT;
}
