package za.co.velvetant.taxi.engine.api.coupon;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import za.co.velvetant.taxi.engine.api.Customer;

import java.util.Date;

public class CustomerCoupon extends Coupon {

    private Date redemptionDate;

    private Customer customer;

    public CustomerCoupon() {
    }

    public CustomerCoupon(final String uuid, final Coupon coupon, final Customer customer) {
        super(coupon.getUuid(),
                coupon.getCouponNumber(),
                coupon.getDescription(),
                coupon.getValidFrom(),
                coupon.getValidTo(),
                coupon.getMonetaryLimit(),
                coupon.getUsageLimit());
        this.setUuid(uuid);
        this.customer = customer;
    }

    public CustomerCoupon(final String uuid, final Coupon coupon, final Customer customer, final Date redemptionDate) {
        this(uuid, coupon, customer);
        this.redemptionDate = redemptionDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Date getRedemptionDate() {
        return redemptionDate;
    }

    public void setRedemptionDate(final Date redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getUuid()).hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if(obj instanceof CustomerCoupon) {
            equals = new EqualsBuilder().append(getUuid(), ((CustomerCoupon) obj).getUuid()).isEquals();
        }

        return equals;
    }
}
