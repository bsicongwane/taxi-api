package za.co.velvetant.taxi.engine.api;

import java.util.ArrayList;
import java.util.List;

public class Company extends Base {

    private String companyName;
    private String billingAddress1;
    private String billingAddress2;
    private String billingAddress3;
    private String billingPostalCode;
    private String postalAddress1;
    private String postalAddress2;
    private String postalAddress3;
    private String postalPostalCode;

    private String registration;
    private String vat;
    private Boolean taxExempt;
    private Boolean active;

    private List<String> administratorEmails = new ArrayList<String>();

    public Company() {
    }

    public Company(final String companyName, final String billingAddress1, final String billingAddress2, final String billingAddress3, final String billingPostalCode, final String postalAddress1,
            final String postalAddress2, final String postalAddress3, final String postalPostalCode,

            final String registration, final String vat, final Boolean taxExempt) {
        this.companyName = companyName;
        this.billingAddress1 = billingAddress1;
        this.billingAddress2 = billingAddress2;
        this.billingAddress3 = billingAddress3;
        this.billingPostalCode = billingPostalCode;
        this.postalAddress1 = postalAddress1;
        this.postalAddress2 = postalAddress2;
        this.postalAddress3 = postalAddress3;
        this.postalPostalCode = postalPostalCode;

        this.registration = registration;
        this.vat = vat;
        this.taxExempt = taxExempt;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(final String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(final String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingAddress3() {
        return billingAddress3;
    }

    public void setBillingAddress3(final String billingAddress3) {
        this.billingAddress3 = billingAddress3;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(final String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    public String getPostalAddress1() {
        return postalAddress1;
    }

    public void setPostalAddress1(final String postalAddress1) {
        this.postalAddress1 = postalAddress1;
    }

    public String getPostalAddress2() {
        return postalAddress2;
    }

    public void setPostalAddress2(final String postalAddress2) {
        this.postalAddress2 = postalAddress2;
    }

    public String getPostalAddress3() {
        return postalAddress3;
    }

    public void setPostalAddress3(final String postalAddress3) {
        this.postalAddress3 = postalAddress3;
    }

    public String getPostalPostalCode() {
        return postalPostalCode;
    }

    public void setPostalPostalCode(final String postalPostalCode) {
        this.postalPostalCode = postalPostalCode;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(final String registration) {
        this.registration = registration;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(final String vat) {
        this.vat = vat;
    }

    public Boolean getTaxExempt() {
        return taxExempt;
    }

    public void setTaxExempt(final Boolean taxExempt) {
        this.taxExempt = taxExempt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    public List<String> getAdministratorEmails() {
        return administratorEmails;
    }

    public void setAdministratorEmails(final List<String> administratorEmails) {
        if (administratorEmails == null) {
            this.administratorEmails = new ArrayList<String>();
        } else {
            this.administratorEmails = administratorEmails;
        }
    }
}
