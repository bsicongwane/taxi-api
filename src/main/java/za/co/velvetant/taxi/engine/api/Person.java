package za.co.velvetant.taxi.engine.api;

import java.util.Date;

public abstract class Person extends Base {

    private String firstName;
    private String lastName;
    private String cellphone;
    private String email;
    private String password;
    private String userId;
    private Date createdDate;

    public Person() {
    }

    public Person(final String firstName, final String lastName, final String cellphone, final String email, final String userId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cellphone = cellphone;
        this.email = email;
        this.userId = userId;
    }

    protected Person(String firstName, String lastName, String cellphone, String email, String userId, Date createdDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cellphone = cellphone;
        this.email = email;
        this.userId = userId;
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCellphone() {
        return cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public void setCellphone(final String cellphone) {
        this.cellphone = cellphone;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
