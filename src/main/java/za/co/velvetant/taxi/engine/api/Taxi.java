package za.co.velvetant.taxi.engine.api;

public class Taxi extends Base {

    private String taxiId;
    private String registration;
    private String make;
    private String model;
    private String color;
    private Integer capacity;
    private String status;

    private DriverBox driverBox;

    public Taxi() {
    }

    public Taxi(final String registration) {
        this.registration = registration;
    }

    public Taxi(final String taxiId, final String registration, final String make, final String model, final String color, final Integer capacity) {
        this.taxiId = taxiId;
        this.registration = registration;
        this.make = make;
        this.model = model;
        this.color = color;
        this.capacity = capacity;
        this.status = "OFFLINE";
    }

    public String getTaxiId() {
        return taxiId;
    }

    public void setTaxiId(final String taxiId) {
        this.taxiId = taxiId;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(final String registration) {
        this.registration = registration;
    }

    public String getMake() {
        return make;
    }

    public void setMake(final String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(final Integer capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public DriverBox getDriverBox() {
        return driverBox;
    }

    public void setDriverBox(final DriverBox driverBox) {
        this.driverBox = driverBox;
    }
}
