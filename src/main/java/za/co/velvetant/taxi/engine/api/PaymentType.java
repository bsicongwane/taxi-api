package za.co.velvetant.taxi.engine.api;

public class PaymentType extends Base {

    private PaymentMethod method;
    private String cvv;
    public PaymentType () {
        
    }
    
    public PaymentType(PaymentMethod method, String cvv) {
        super();
        this.method = method;
        this.cvv = cvv;
    }

    public PaymentMethod getMethod() {
        return method;
    }

    public void setMethod(PaymentMethod method) {
        this.method = method;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

}
