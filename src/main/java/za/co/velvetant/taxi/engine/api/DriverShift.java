package za.co.velvetant.taxi.engine.api;

public class DriverShift extends Base {

    private String driverId;
    private String shiftIdentifier;
    private String firstName;
    private String lastName;

    private String taxiRegistration;

    @SuppressWarnings("unused")
    protected DriverShift() {
        // required by jackson
    }

    public DriverShift(final String driverId, final String taxiRegistration) {

        this.driverId = driverId;
        this.taxiRegistration = taxiRegistration;
    }

    public DriverShift(final String driverId, final String taxiRegistration, final String shiftIdentifier) {

        this.driverId = driverId;
        this.taxiRegistration = taxiRegistration;
        this.shiftIdentifier = shiftIdentifier;
    }

    public DriverShift(final String driverId, final String firstName, final String lastName, final String taxiRegistration) {

        this(driverId, taxiRegistration);

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public DriverShift(final String driverId, final String firstName, final String lastName, final String taxiRegistration, final String shiftIdentifier) {

        this(driverId, taxiRegistration, shiftIdentifier);

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getTaxiRegistration() {
        return taxiRegistration;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getShiftIdentifier() {
        return shiftIdentifier;
    }
}
