package za.co.velvetant.taxi.engine.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("send")
public class Sms implements Serializable {

    private Send send;

    public Sms() {
    }

    public Sms(final Message message) {
        this.send = new Send(message);
    }

    public Send getSend() {
        return send;
    }

    public static class Message implements Serializable {

        private String to;
        private String reference;
        private String content;

        public Message() {
        }

        public Message(final String to, final String reference, final String content) {
            this.to = to;
            this.reference = reference;
            this.content = content;
        }

        public String getTo() {
            return to;
        }

        public String getReference() {
            return reference;
        }

        public String getContent() {
            return content;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    static class Send implements Serializable {

        private List<Message> messages;

        public Send() {
        }

        public Send(final Message message) {
            this.messages = new ArrayList<Message>();
            messages.add(message);
        }

        public List<Message> getMessages() {
            return messages;
        }

    }

}
