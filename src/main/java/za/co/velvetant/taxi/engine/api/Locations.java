package za.co.velvetant.taxi.engine.api;

import za.co.velvetant.taxi.api.common.Location;

public class Locations extends Base {

    private Location pickup;
    private Location dropOff;

    public Locations() {
    }

    public Locations(final Location pickup, final Location dropOff) {
        this.pickup = pickup;
        this.dropOff = dropOff;
    }

    public Location getPickup() {
        return pickup;
    }

    public void setPickup(final Location pickup) {
        this.pickup = pickup;
    }

    public Location getDropOff() {
        return dropOff;
    }

    public void setDropOff(final Location dropOff) {
        this.dropOff = dropOff;
    }

}
