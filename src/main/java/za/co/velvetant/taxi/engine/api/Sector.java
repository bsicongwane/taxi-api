package za.co.velvetant.taxi.engine.api;

public class Sector extends BaseId {

    private String name;

    public Sector() {
    }

    public Sector(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
