package za.co.velvetant.taxi.engine.api;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Customer extends Person {

    private Boolean active;
    private Boolean sendNewsLetter = false;
    private Long snappMiles;
    private List<String> paymentMethods;

    private String corporateRegistration;
    private String familyName;
    private String familyUuid;

    private String administeredCorporate;
    private String administeredFamily;

    private String hash;
    private BigDecimal balance;

    public Customer() {
    }

    public Customer(final String firstName, final String lastName, final String cellphone, final String email) {
        super(firstName, lastName, cellphone, email, email);
    }

    public Customer(String firstName, String lastName, String cellphone, String email, Date createdDate) {
        super(firstName, lastName, cellphone, email, email, createdDate);
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    public Boolean getSendNewsLetter() {
        return sendNewsLetter;
    }

    public void setSendNewsLetter(final Boolean sendNewsLetter) {
        this.sendNewsLetter = sendNewsLetter;
    }

    public Long getSnappMiles() {
        return snappMiles;
    }

    public void setSnappMiles(final Long snappMiles) {
        this.snappMiles = snappMiles;
    }

    public List<String> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(final List<String> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getCorporateRegistration() {
        return corporateRegistration;
    }

    public void setCorporateRegistration(final String corporateRegistration) {
        this.corporateRegistration = corporateRegistration;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(final String familyName) {
        this.familyName = familyName;
    }

    public String getFamilyUuid() {
        return familyUuid;
    }

    public void setFamilyUuid(final String familyUuid) {
        this.familyUuid = familyUuid;
    }

    public String getAdministeredCorporate() {
        return administeredCorporate;
    }

    public void setAdministeredCorporate(final String administeredCorporate) {
        this.administeredCorporate = administeredCorporate;
    }

    public String getAdministeredFamily() {
        return administeredFamily;
    }

    public void setAdministeredFamily(final String administeredFamily) {
        this.administeredFamily = administeredFamily;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(final BigDecimal balance) {
        this.balance = balance;
    }
}
