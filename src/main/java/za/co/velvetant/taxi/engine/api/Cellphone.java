package za.co.velvetant.taxi.engine.api;

public class Cellphone {

    public Cellphone() {

    }
    public Cellphone(final String cellphone) {
        this.cellphone = cellphone;
    }

    private String cellphone;

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(final String cellphone) {
        this.cellphone = cellphone;
    }
}
