package za.co.velvetant.taxi.engine.api.transactions;

import za.co.velvetant.taxi.engine.api.Base;
import za.co.velvetant.taxi.engine.api.Customer;
import za.co.velvetant.taxi.engine.api.Fare;

import java.math.BigDecimal;
import java.util.Date;

public class Transaction extends Base {

    private String uuid;
    private Date date;
    private TransactionType type;
    private BigDecimal amount;

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public void setType(final TransactionType type) {
        this.type = type;
    }

    private Customer customer;
    private Fare trip;

    @SuppressWarnings("UnusedDeclaration")
    public Transaction() {
    }

    public Transaction(final String uuid, final Date date, final TransactionType type, final BigDecimal amount) {
        this.uuid = uuid;
        this.date = date;
        this.type = type;
        this.amount = amount;
    }

    public Transaction(final String uuid, final Date date, final TransactionType type, final BigDecimal amount, final Customer customer) {
        this(uuid, date, type, amount);
        this.customer = customer;
    }

    public Transaction(final String uuid, final Date date, final TransactionType type, final Fare trip) {
        this(uuid, date, type, new BigDecimal(trip.getAmount()), trip.getCustomer());
        this.trip = trip;
    }

    public String getUuid() {
        return uuid;
    }

    public Date getDate() {
        return date;
    }

    public TransactionType getType() {
        return type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Fare getTrip() {
        return trip;
    }

    public void setTrip(final Fare trip) {
        this.trip = trip;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
