package za.co.velvetant.taxi.engine.api.transactions;

public enum TransactionType {

    CASH, CREDIT_CARD, COUPON;
}
