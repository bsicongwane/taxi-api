package za.co.velvetant.taxi.engine.api;

import java.util.Date;

public class Rating extends Base {

    private int tripRating;
    private int driverRating;
    private String fare;
    private String customer;
    private Date date;
    private String feedback;

    public Rating() {
    }

    public Rating(final int tripRating, final int driverRating, final String fare) {
        this.tripRating = tripRating;
        this.driverRating = driverRating;
        this.fare = fare;
    }
    public Rating(final int tripRating, final int driverRating, final String fare, final String feedback) {
        this(tripRating, driverRating, fare);
        this.feedback = feedback;
    }

    public Rating(int tripRating, int driverRating, String fare, String customer, Date date, String feedback) {
        this.tripRating = tripRating;
        this.driverRating = driverRating;
        this.fare = fare;
        this.customer = customer;
        this.date = date;
        this.feedback = feedback;
    }

    public int getTripRating() {
        return tripRating;
    }

    public void setTripRating(final int tripRating) {
        this.tripRating = tripRating;
    }

    public int getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(final int driverRating) {
        this.driverRating = driverRating;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(final String fare) {
        this.fare = fare;
    }

    public String getCustomer() {
        return customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(final String feedback) {
        this.feedback = feedback;
    }
}

